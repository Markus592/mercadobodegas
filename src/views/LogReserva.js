import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'

// import LoginReserva from './components/FormReserva'
export default class LogReserva extends Component {
    state = {
        email:'',
        codigo:'',
        respuesta:{}
        
    }
    loginincorrecto(){
        console.log(this.state.respuesta)
        alert("login incorrecto");
        window.location.replace('')
    
    }
    handleChange = event => {
        if(event.target.name=="codigo"){
            this.setState({ codigo: event.target.value });
            
        }
        else{
            if(event.target.name=="email"){
                this.setState({ email: event.target.value });
        
            }
        }
      }
      
    handleSubmit = event => {
        event.preventDefault();
     
        const usuario = {
            login_correo: this.state.email,
            login_codigo: this.state.codigo
    
        };
        var myJsonString = JSON.stringify(usuario);
     
        // axios.post(`https://jsonplaceholder.typicode.com/users`, { usuario })
        // axios.post(`http://192.168.1.14/z_proyectosLaravel/MercadoBodegasDos/public/api/reservas/insertarReserva`, JSON.stringify(usuario))
        // console.log(myJsonString);
        
        console.log(myJsonString);
        axios.post(`http://amdigital.tech/api/reservas/loginModificarReserva`, myJsonString)
          .then(res => {
            this.setState({respuesta:res.data})
            
            console.log(res.data);
          })
          
      }
    render() {
        return (
            
            <div>
               
                {/* <LoginReserva        
                    titulo="Logeate para Actualizar Reserva"
                    type1="text"
                    type2="number"
                    type3="email"
                    name1="username"
                    name2="password"
                    name3="email"
                    plac1="Codigo de la Reserva"
                    plac2="Tel&eacute;fono"
                    plac3="Email"
                    namebtn="Login"
                    click={this.funcion}
                /> */}
                <div className="wrapper">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                        <h2 className="form-signin-heading">Logeate para Actualizar Reserva </h2>
                        
                        <input type="text" className="form-control" name="codigo" onChange={this.handleChange} placeholder="Codigo de la Reserva" required=""/>      
                        <input type="email" className="form-control" name="email" onChange={this.handleChange} placeholder="email" required=""/>      
                        {/* <button   className="btn updreserva btn-lg bg-orange btn-block" onClick={this.click.bind(this)} type="submit" >{this.props.namebtn}  </button> */}
                        <button   className="btn updreserva btn-lg bg-orange btn-block" type="submit" >LOGIN</button>
                </form>
                {this.state.respuesta.mensaje=="login correcto" ?
                
                <div>
                    <Redirect to={"/actualizar-reserva/"+this.state.codigo}/>
                </div>

                :
                this.state.respuesta.mensaje=="login incorrecto" ?
                
                    
                   this.loginincorrecto()
                
                
                :null
                
                
                }
                
                
            </div>

            </div>
        )
    }
}
