
import React, { Component } from 'react'
import axios from 'axios'
import Header from './components/navbar'
import Footer from './components/footer'
import logo_header from './img/logoheader.svg'
import logo_footer from './img/logofooter.svg'
import Formulario from './components/formulario'
import Iframe from 'react-iframe'
export default class services_detalles extends Component
{
    filtro(n)
    {   
        let f=this.state.filtro;
        let x=[];
        let indice=f.indexOf(n);
        if(n==3){
            x.push(0,(f[indice]+f[indice+1])/2);
            this.setState({numero:x}) 
            // console.log("estoes 3");
        }
        else {
            if(n==30){
            this.setState({numero:n});
            // console.log("esto es 30");
        }
        else{
            x.push(f[indice-1],(f[indice]+f[indice+1])/2);
            this.setState({numero:x}) 
            // console.log("lo demas ");
            
        }
        
    }
    
    }
    state =
    {
        detalle:{},
        numero:[],
        id:0,
        caracteristicasmod:{},
        filtro:[3,5,7,9,10,15,20,30]
        
        
    }
     groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
          const key = keyGetter(item);
          const collection = map.get(key);
          if (!collection) {
            map.set(key, [item]);
          } else {
            collection.push(item);
          }
        });
        return map;
      }
    convertir(array){
        let a=[];
        let b=[];
        let c=[];
        let d=[];
        let arreglo=[];
        
        if(array!=undefined)
        {
            array.forEach(ar =>
            {
                a.push({
                    nombre:ar.caracteristicasLocal_nombre,
                    grupo:ar.grupo_caracteristicas.grupo_nombre
                })
                b.push(ar.grupo_caracteristicas.grupo_nombre)
            })
    }
    let sinRepetidos = b.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
            valorDelArreglo =>JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
            ) === indiceActual
    });
    const x=this.groupBy(a,p=>p.grupo)
    sinRepetidos.map(sr=>{
        
        c.push(x.get(sr));
    })
    c.map(e=>{
        d.push(e);
    })
    
    
        d.map(item=>{
        
        item.map(it=>{
                arreglo.push({grupo:it.grupo,item:item})
        })
    })
    let sinRepetidoshard = arreglo.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
            valorDelArreglo =>JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
            ) === indiceActual
    });
    
    return sinRepetidoshard;
    
    // this.setState({caracteristicasmod:sinRepetidoshard})
    
   
}
     async componentDidMount()
    {
        // axios.get('http://localhost:4001/content')
        // axios.get('http://localhost:4001/detalles')
        
        // let id = this.props.match.params.id;
        
        let arreglo=[];
       await axios.get('http://amdigital.tech/api/locales/'+this.props.id)
        .then
            (result=>
                {
                this.setState({detalle:result.data});
                arreglo=result.data;
                }
            )
        .catch(error=>console.log(error))
        
        

        this.setState({caracteristicasmod:this.convertir(arreglo.caracteristicas)})
                
        
        
        
    }
    render()
    {
        // console.log(this.state.caracteristicasmod)
        
        const url = "https://www.mercadobodegas.cl/almacenes/public/img/instalaciones"
    var array ={}
        // if(this.state.detalle.t_galeria != undefined)
        //     {            
            return (
                <div>
                    
                    <Header logo={logo_header} 
                        item1="Bodegas cerca tuyo"
                        item2="Guía de tamaños"
                        item3="Contacto"/>                        
                    <div className="container mt-3 mb-5">
                      <div className="row mt-3">
                        <div className="col-md-6">
                            <h3 className="mt-4">
                                <b>{this.state.detalle.local_nombre}</b>
                            </h3>
                            <span className="mr-3">
                                {this.state.detalle.local_direccion}
                            </span>
                            <span className="d-xs-block link-border border-white">
                                <b >
                                    Telefono: {this.state.detalle.local_telefono}
                                </b>
                            </span>
                            
                        </div>

                        <div  className="col-md-6">
                            <h4>Haz tu reserva sin riesgos para asegurar una tarifa baja,
                                sin pagar hasta tu mudanza.
                            </h4>
                            <h6>
                                Cancela tu reserva sin costo.
                            </h6>
                            
                        </div>
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-md-6">
                                <div id="carouselExampleIndicators" className="carousel slide  mt-3 mb-3" data-ride="carousel" >
                                        {/* <img className="img-fluid" src={ url+"/"+this.state.detalle.t_galeria[0].id_instalacion+"/"+this.state.detalle.t_galeria[0].url} alt ="imagen "></img> */}
                                        <ol className="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                        </ol>
                        <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100" src="https://picsum.photos/200/300?random=1" alt="First slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="https://picsum.photos/200/300.jpg" alt="Second slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="https://picsum.photos/200/300/?blur" alt="Third slide"/>
                        </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                        </a>
                                    </div>
                                    <div>
                                        <h6>
                                            <b>
                                            Sobre {this.state.detalle.local_nombre}
                                            </b>

                                        </h6>
                                        <div className="descripcion expand-content collapsed" 
                                            id="descripcion">
                                            <p  className="ph-paragraph small light">
                                                <span > 
                                                    {this.state.detalle.local_descripcion}
                                                </span>
                                            </p>
                                        </div>
                                        <div >
                                            <h5 >
                                                <span className="border-bottom border-black">
                                                    <a id="ver-detalles">Ver más</a>
                                                </span>
                                            </h5>
                                        </div>
                                    </div>
                                    <div className="amenidades lg-amenidades mt-3">
                                        <h4>
                                            <b>
                                                Características
                                            </b>
                                        </h4>
                                       
                                        <div  className="caracteristicas-amenidades row mt-4">

                                            {/* {this.state.caracteristicasmod.length=0 ?
                                            this.setState({caracteristicasmod:this.convertir(this.state.detalle.caracteristicas)})
                                            
                                            :console.log("no se pudo poner ")}
                                            {console.log(this.state.caracteristicasmod)} */}
                                            {/* {console.log(array)} */}
                                        
                                            {/* {console.log("vamos a empezar")} */}
                                            {this.state.caracteristicasmod.map!=undefined?
                                            this.state.caracteristicasmod.map( am=>
                                                    {
                                                        return(
                                                            <div key={am.grupo} className="col-sm-6">
                                                                <div>
                                                                    <h6>
                                                                        <b>
                                                                            {am.grupo}
                                                                        </b>
                                                                    </h6>
                                                                    <ul>
                                                                        {/* {console.log(am.item)} */}
                                                                        {
                                                                        am.item.map(
                                                                        it=>{
                                                                            
                                                                            
                                                                            return(
                                                                                <li key={it.nombre}>
                                                                                    <i className="check fa fa-check" aria-hidden="true"></i>
                                                                                    {it.nombre}
                                                                                </li>
                                                                            )
                                                                    })}
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            )
                                                    }
                                                ):null
                                            } 
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                    <h6>
                                                            <b>
                                                            Horarios de oficina
                                                            </b>
                                                        </h6>
                                                        {
                                    
                                                        this.state.detalle.horario!=undefined?
                                    this.state.detalle.horario.map
                                    (
                                        hor=>
                                        {
                                            return(                                                
                                                      <div className="col-sm-12" key={hor.horario_id}>
                                                        
                                                            {hor.horario_tipo==="o"
                                                                ?
                                                                <div className="row mb-2">
                                                                    <div className="col-md-5">
                                                                        <span>
                                                                        {hor.horario_dia}
                                                                        </span>

                                                                    </div>
                                                                    <div className="col-md-7">
                                                                        <span>
                                                                        {hor.horario_horaEntrada+" - "+hor.horario_horaSalida}
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                                
                                                                    
                                                                
                                                                :null
                                                                
                                        }
                                                    
                                                      </div>                                                                 
                                                    )
                                        }
                                    ):null
                                }
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <h6>
                                                            <b>
                                                                Horarios de acceso
                                                            </b>
                                                        </h6>
                                                        {
                                    
                                                            this.state.detalle.horario!=undefined?
                                    this.state.detalle.horario.map
                                    (
                                        hor=>
                                        {
                                            return(                                                
                                                      <div className="col-sm-12" key={hor.horario_id}>
                                                        
                                                            {hor.horario_tipo==="a"
                                                                ?
                                                                <div className="row mb-2">
                                                                    <div className="col-md-5">
                                                                        <span>
                                                                        {hor.horario_dia}
                                                                        </span>

                                                                    </div>
                                                                    <div className="col-md-7">
                                                                        <span>
                                                                        {hor.horario_horaEntrada+" - "+hor.horario_horaSalida}
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                                
                                                                    
                                                                
                                                                :null
                                                                
                                        }
                                                    
                                                      </div>                                                                 
                                                    )
                                        }
                                    ):null
                                }
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>

                                    </div>
                                  
                                </div>
                                <div className="col-md-6">
                                <h4> <a className="text-dark" href="#"> Unidades (Guía de tamaños) </a></h4>
                            <div className="filtros-botones">
                                <div   className="header-unidades bg-grey p-2">
                                    <button
                                        className="btn btn-border mb-1 mt-1  ng-star-inserted"
                                        id="buttonFiltro0" 
                                        onClick={this.filtro.bind(this,3)}
                                        >
                                            3 m
                                            <sup>2</sup>
                                            
                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary "
                                        id="buttonFiltro1"
                                        onClick={this.filtro.bind(this,5)}

                                        >
                                            5 m
                                            <sup>2</sup>

                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary ng-star-inserted"
                                        id="buttonFiltro2"
                                        onClick={this.filtro.bind(this,7)}
                                        >
                                            7 m
                                            <sup>2</sup>
                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary ng-star-inserted"
                                        id="buttonFiltro3"
                                        onClick={this.filtro.bind(this,10)}>
                                            10 m
                                            <sup>2</sup>
                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary ng-star-inserted"
                                        id="buttonFiltro4"
                                        onClick={this.filtro.bind(this,15)}>
                                            15 m
                                            <sup>2</sup>
                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary ng-star-inserted"
                                        id="buttonFiltro5"
                                        onClick={this.filtro.bind(this,20)}>
                                            20 m
                                            <sup>2</sup>
                                    </button>
                                    <button
                                        className="btn btn-border mb-1 mt-1 bg-primary ng-star-inserted"
                                        id="buttonFiltro6"
                                        onClick={this.filtro.bind(this,30)}
                                        >
                                            30 m
                                            <sup>2</sup>
                                    </button>
                                    </div>
                                    {this.state.detalle.unidad!=undefined?
                                    this.state.detalle.unidad.map(
                                            unid=>{
                                                // console.log(this.state.detalle.unidad);
                                                return(
                                                    
                                                    this.state.numero==0?
                                                        <div key={unid.unidad_id}>
                                                            <div    className="row mb-2 row-reserva d-flex p-3  bd-highlight" 
                                                                    data-toggle="collapse"
                                                                    href={"#multiCollapseExample" +unid.unidad_id}
                                                                    role="button" 
                                                                    aria-expanded="false"
                                                                    aria-controls={"multiCollapseExample"+unid.unidad_id}>
                                                                <div className="col-md6" >
                                                                    <b>
                                                                        Unidad {unid.unidad_area}
                                                                        <sup>2</sup>
                                                                    </b>
                                                                </div>
                                                                <div className="col-md6">
                                                                    <div className="precio">
                                                                        {unid.unidad_precioMensual}        
                                                                    </div> 
                                                                    <div>
                                                                        por mes 
                                                                    </div>
                                                                </div>
                                                                <div className="col-md6" >
                                                                    <button className="btn btn-reserva">
                                                                        Continuar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col">
                                                                    <div
                                                                        className="collapse " 
                                                                        id={"multiCollapseExample"+unid.unidad_id }>
                                                                            <div className="card card-body">
                                                                              
                                                                                <Formulario unidad_id={unid.unidad_id}/>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>:
                                                        <div>
                                                            
                                                        {unid.unidad_area>=this.state.numero[0] && unid.unidad_area <= this.state.numero[1] ?
                                                        
                                                            <div  key={unid.unidad_id}>
                                                                <div    className="row mb-2 row-reserva d-flex p-3 bd-highlight"
                                                                    data-toggle="collapse"
                                                                    href={"#multiCollapseExample" +unid.unidad_id}
                                                                    role="button"
                                                                    aria-expanded="false"
                                                                    aria-controls={"multiCollapseExample"+unid.unidad_id}>
                                                                <div className="col-md6" >
                                                                    <b>
                                                                        Unidad {unid.unidad_area}
                                                                        <sup>2</sup>
                                                                    </b>
                                                                </div>
                                                                <div className="col-md6">
                                                                    <div className="precio">
                                                                        {unid.unidad_precioMensual}        
                                                                    </div> 
                                                                    <div>
                                                                        por mes
                                                                    </div>
                                                                </div>
                                                                <div className="col-md6" >
                                                                    <button className="btn btn-reserva">
                                                                        Continuar
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col">
                                                                    <div
                                                                        className="collapse " 
                                                                        id={"multiCollapseExample"+unid.unidad_id }>
                                                                            <div className="card card-body">
                                                                            <Formulario unidad_id={unid.unidad_id}/>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            :null }
                                                    </div>        
                                                        )
                                                }
                                        ):console.log("las unidades estan indefinidas")
                                    }
                                
                                <div className="row ml-2 mb-4">
                                  
                                    <Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d956.9078340499422!2d-71.54434048924757!3d-16.39273197873491!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a4347b44d6d%3A0x96fc1092a2510f!2sAtahualpa%20105!5e0!3m2!1ses-419!2spe!4v1578878894898!5m2!1ses-419!2spe"
                                            width="95%"
                                            height="250px"
                                            id="myId"
                                            className="myClassname"
                                            display="initial"
                                            position="relative"/>
                                </div>
                            </div>
                                </div>

                            </div>
                        </div>
                          
                       
                    </div>
                    </div>
                    <Footer
                        item1="¿Necesitas ayuda?"
                        item2="Contacto"
                        item3=" contacto@mercadobodegas.net"
                        item4="09807169267"
                        item5="Para dueños de Mini Bodegas"
                        item6="Manual de Uso"
                        item7="Iniciar Sesión"
                        item8="MercadoBodegas en:"
                        item9="© MercadoBodegas 2019 | Derechos Reservados"
                        logo={logo_footer}
                    />
                </div>
                )
            } 
            // else
            // {
            //     return  (
            //                 <div>
            //                     {console.log(this.state.detalle.t_galeria)}
            //                     {this.state.detalle.nombreInstalacion}
            //                     {/* {this.state.detalle.t_galeria!=undefined?this.state.detalle.t_galeria[0].url:null} */}
            //                 </div>
            //             )
            // }
    }
