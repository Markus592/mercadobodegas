import React from "react";
import {
  BrowserRouter as Router,
 
  useParams
} from "react-router-dom";
import UpdReserva from '../updatereserva'


export default function Child() {

  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.
  let { id } = useParams();

  return (
    <div>
      {console.log({id})}
        <UpdReserva id={id}/>
      
    </div>
  );
}