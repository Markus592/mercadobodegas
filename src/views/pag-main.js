import React from 'react';
import './css/index.css';
import 'react-google-places-autocomplete/dist/assets/index.css';
import logo_header from './img/logoheader.svg'
import logo_footer from './img/logofooter.svg'
import Header from './components/navbar'
import Search from './components/search';
import Footer from './components/footer';
class pag_main extends React.Component {
  render()
  {
    
 return (
   <div>
    <Header logo={logo_header} 
  item1="Bodegas cerca tuyo"
  item2="Guía de tamaños"
  item3="Contacto"/>
  <div   className="container pt-5 pb-5 text-center"
                  id="headerbajo">
                    <h1  className="mt-5 mb-2">
                    Todas las mini bodegas en un solo lugar.
                    </h1>
                    <h3 className="mb-4">
                    Cotiza, compara y reserva sin compromiso.
                    </h3>
  <div className="form-inline my-2 my-lg-0 row text-center">
  <Search 
  clasebutton="btn btn-block bg-black white my-2 my-sm-0"
  placeholder="Ingresa dirección, calle, comuna, o ciudad."
  txtbutton="Buscar"
  />
  </div>
  </div>
  <section
      className="mt-5 mb-5 pt-4 bg-white"
      id="caracteristicas"
    >
      <div   className="container-fluid text-center">
        <div   className="container">
          <div   className="row">
            <div   className="text-center w-100">
              <h3   className="mb-2">
                Prueba nuestro buscador y verás lo fácil que es reservar sin
                costo tu mini bodega.
              </h3>
            </div>
            <p
               
              className="mt-2 mb-5 col-md-8 offset-md-2"
            >
              MercadoBodegas te permite comparar y filtrar los resultados de tu
              búsqueda según precios, tamaños y otras características. Elige la
              mejor opción y luego reserva sin necesidad de registrarte.
            </p>
          </div>
          <div   className="row mt-4">
            <div   className="d-flex flex-column col-sm-3">
              <i   className="fa fa-search fa-5x mb-3"></i><span   className="texto-ayuda">
                Busca y selecciona una bodega
              </span>
            </div>
            <div   className="d-flex flex-column col-sm-3">
            <i class="fas fa-ticket-alt fa-5x mb-3"></i>
              <span   className="texto-ayuda">
                Reserva gratis en línea
              </span>
            </div>
            <div   className="d-flex flex-column col-sm-3">
            <i class="fas fa-warehouse fa-5x mb-3"></i><span   className="texto-ayuda">
                Visita la bodega
              </span>
            </div>
            <div   className="d-flex flex-column col-sm-3">
              <i
                 
                className="fas fa-people-carry fa-5x mb-3"></i><span   className="texto-ayuda">
                Lleva tus cosas
              </span>
            </div>
          </div>
        </div>
      </div>
    </section>
  <Footer
  item1="¿Necesitas ayuda?"
  item2="Contacto"
  item3=" contacto@mercadobodegas.net"
  item4="09807169267"
  item5="Para dueños de Mini Bodegas"
  item6="Manual de Uso"
  item7="Iniciar Sesión"
  item8="MercadoBodegas en:"
  item9="© MercadoBodegas 2019 | Derechos Reservados"
  logo={logo_footer}
  />

<div>
  
   
  </div>
  </div>
)
}
 
}
export default pag_main;

