// import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
// import React, { Component } from 'react'
// var points = [
//     { lat: 42.02, lng: -77.01 },
//     { lat: 42.03, lng: -77.02 },
//     { lat: 41.03, lng: -77.04 },
//     { lat: 42.05, lng: -77.02 }
// ]


// export class MapContainer extends Component {
    
//   render() {
//     return (
//       <Map google={this.props.google} zoom={11}
//       initialCenter={{
//         lat: -33.4726900,
//         lng: -70.6472400
//       }}
//       bounds={points}
//       >
 
//         <Marker onClick={this.onMarkerClick}
//                 name={'Current location'} />
 
//         <InfoWindow onClose={this.onInfoWindowClose}>
//             <div>
//               {/* <h1>{this.state.selectedPlace.name}</h1> */}
//             </div>
//         </InfoWindow>
//       </Map>
//     );
//   }
// }
 
// export default GoogleApiWrapper({
//   apiKey: "AIzaSyBfl5JsPWncvaMJd0icji5e0huUNWh2aEo"
// })(MapContainer)



import React,{useState} from 'react'
import {
    withGoogleMap,
    withScriptjs,
    GoogleMap,
    Marker,
    InfoWindow
  } from "react-google-maps"
//   import * as Data from '../data/skateboard-parks'
import * as Data from '../data/locales'
  import Mapstyle from './css/mapstyle'
  
  
function Map(){
    const [selectedPark,setSelectPark]= useState(null);
    return <GoogleMap
    defaultZoom={10}
    defaultCenter={{ lat: -33.4569397,
    lng: -70.6482697}}
    defaultOptions={{styles:Mapstyle}}
    >
        {Data.locales.map(dat=>
        <Marker
            key={dat.local_id}
            position={{
                lat:dat.local_latitud,
                lng:dat.local_longitud
            }}
            onClick={()=>{
                setSelectPark(dat);
            }}
            icon={{
                url:'/logomap.png',
                
            scaledSize: new window.google.maps.Size(20, 20)
            }
            }
        />
        )}
        {
            selectedPark && (
                <InfoWindow
                position={{
                    lat:selectedPark.local_latitud,
                lng:selectedPark.local_longitud
                }}
                onCloseClick={()=>{
                    setSelectPark(null);
                }}
                >
                    <div>
            <h2>{selectedPark.local_nombre}</h2>
            
          </div>
                </InfoWindow>
            )
        }
    </GoogleMap>
}

const key="AIzaSyBfl5JsPWncvaMJd0icji5e0huUNWh2aEo"
const MapWrapped = withScriptjs(withGoogleMap(Map));
export default function mapa() {
    return (
        <div className="mapa">
            <MapWrapped 
                googleMapURL={'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key='+key}
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `100%` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
        </div>
    )
    
}


