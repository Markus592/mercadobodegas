import React, { Component } from 'react'
// import FormReserva from './components/FormReserva'
import axios from 'axios'

export default class updatereserva extends Component {

    funcion(){
        alert("hola");
    }
    state={
        reservas:{}
    }
    componentDidMount()
    {
        axios.get('http://amdigital.tech/api/reservas/buscarReserva/'+this.props.id)
        .then
            (result=>
                {
                    this.setState({reservas:result.data})
                    console.log(this.state.reservas)
                }
            )
        .catch(error=>console.log(error))
    }
    render() {
        var reserva = this.state.reservas;
        return (
            <div>
                {/* <FormReserva
                action="envia.php"
                titulo="Actualiza tu Reserva"
                type1="text"
                type2="number"
                type3="date"
                name1="nombre"
                name2="telefono"
                name3="fecha_mudanza"
                plac1="Nombre"
                plac2="Telefono"
                plac3="Fecha de Mudanza"
                namebtn="Actualizar"
                click={this.funcion}
                
                /> */}
                <div className="wrapper">
                <form className="form-signin"  >
                        <h2 className="form-signin-heading">Actualiza tu Reserva</h2>
                        <input type="text" value={reserva.reserva_nombre} className="form-control" name="nombre" placeholder="Nombre" required="" autoFocus="" />
                        <input type="number" value={reserva.reserva_telefono} className="form-control" name="telefono" placeholder="Telefono" required=""/>      
                        <input type="date" value={reserva.reserva_fechaMudanza} className="form-control" name="fecha_mudanza" placeholder="fecha de mudanza" required=""/>      
                        <button   className="btn updreserva btn-lg bg-orange btn-block" type="submit" >Actualizar  </button>
                </form>
            </div>
            </div>
        )
    }
}
