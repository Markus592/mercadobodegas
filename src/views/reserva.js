import React, { Component } from 'react'
import axios from 'axios'
import Header from './components/navbar'
import Footer from './components/footer'
import logo_header from './img/logoheader.svg'
import logo_footer from './img/logofooter.svg'
import RowConfirReserva from './components/reserva_confirmada'
import InfoReserva from './components/informacion_reserva'

export default class reserva extends Component {
    state={
        reservas:{}
    }
    componentDidMount()
    {

        axios.get('http://amdigital.tech/api/reservas/buscarReserva/'+this.props.id)
        .then
            (result=>
                {
                    this.setState({reservas:result.data})
                    console.log(this.state.reservas)
                }
            )
        .catch(error=>console.log(error))
    }
    render() {
        
        
        return (
            <div>
                
                <Header logo={logo_header} 
                        item1="Bodegas cerca tuyo"
                        item2="Guía de tamaños"
                        item3="Contacto"/>   
                <RowConfirReserva
                    codigo={this.state.reservas.reserva_codigo}
                    fecha={this.state.reservas.reserva_fechaMudanza}
                />
                <div className="row">
                    <div className="col-sm-9">
                        <div className="card mb-3  bg-white">
                            <div className="card-body">
                                <h3>
                                    Esto es lo que pasa después
                                </h3>
                                <ul  >
                                    <li  >
                                        <i className="check fa fa-check"></i>
                                        Revisa tu bandeja de entrada para recibir un email de confirmación con todos los datos. </li>
                                    <li >
                                    <i className="check fa fa-check"></i>
                                        Te instamos a ponerte en contacto pronto con Mini Bodegas Santiago para coordinar todo tipo de detalles.
                                    </li> 
                                    
                                </ul>
                                <h3 >
                                    Asegúrese de traer lo siguiente
                                </h3>
                                <ul  >
                                    <li  >
                                    <i className="check fa fa-check"></i>
                                        Cédula de identidad.
                                    </li>
                                    <li  >
                                    <i className="check fa fa-check"></i>
                                        Una copia digital o impresa de esta reserva. 
                                    </li>
                                </ul>
                                        
                            </div>
                        </div>
                        
                        <InfoReserva
                        nombre={this.state.reservas.reserva_nombre+" "+this.state.reservas.reserva_apellido}    
                        email={this.state.reservas.reserva_email}    
                        telefono={this.state.reservas.reserva_telefono}
                        id={this.state.reservas.reserva_id}
                        // unidad={this.state.reservas.t_unidade.t_instalacione.nombreInstalacion}
                        // direccion={this.state.reservas.t_unidade.t_instalacione.direccionInstalacion}
                        // region={this.state.reservas.t_unidade.t_instalacione.regionInstalacion}
                        // telefonounidad={this.state.reservas.t_unidade.t_instalacione.telefonoInstalacion}
                        />
                        
                    </div>
                    <div className="col-sm-3">
                        <div   className="card  bg-orange">
                            <div   className="card-body">
                                <div   className="text-center mb-3">
                                    <img
                                        
                                        alt="Mercado Bodegas"
                                        src={logo_header}
                                        className="ng-star-inserted"
                                    />
                                </div>
                                <div  >
                                    <h3  >
                                        Las cosas cambian, lo entendemos.
                                    </h3>
                                    <span>
                                        Si necesitas hacer un cambio en la reserva o tienes alguna duda o
                                        problema, llámanos. Estamos aquí para ayudar.
                                    </span>
                                </div>
                            <div   className="mt-3">
                                <button
                                className="btn bg-black white btn-block">
                                    5695465132
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <Footer
                        item1="¿Necesitas ayuda?"
                        item2="Contacto"
                        item3=" contacto@mercadobodegas.net"
                        item4="09807169267"
                        item5="Para dueños de Mini Bodegas"
                        item6="Manual de Uso"
                        item7="Iniciar Sesión"
                        item8="MercadoBodegas en:"
                        item9="© MercadoBodegas 2019 | Derechos Reservados"
                        logo={logo_footer}
                />
        </div>
        )
        
        
    }
}
