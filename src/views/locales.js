import React, { Component } from 'react'
import logo_header from './img/logoheader.svg'
import logo_footer from './img/logofooter.svg'
import 'react-google-places-autocomplete/dist/assets/index.css';
import './css/index.css';
import Header from './components/navbar'
import Footer from './components/footer'
import axios from 'axios'
import Filtrolateral from './components/filtrolateral'
import Mapa from '../views/mapa'
import Search from '../views/components/search'
export default class locales extends Component {
  filtro(n)
        {   
            let f=this.state.filtro;
            let x=[];
            let indice=f.indexOf(n);
            if(n==3){
                x.push(0,(f[indice]+f[indice+1])/2);
                this.setState({numero:x}) 
                console.log("estoes 3");
            }
            else {
                if(n==30){
                this.setState({numero:n});
                console.log("esto es 30");
            }
            else{
                x.push(f[indice-1],(f[indice]+f[indice+1])/2);
                this.setState({numero:x}) 
                console.log("lo demas ");
                
            }
            
        }
        
        }
    constructor(){
        super();
        this.state = {
            locales:[],
            numero:[],
            actualpagina: 1,
            todosporpagina: 3,
            filtro:[3,5,7,9,10,15,20,30]
        };
        this.handleClick = this.handleClick.bind(this);
        axios.get('http://amdigital.tech/api/locales')
        .then(response =>{
            this.setState({locales:response.data})
            console.log(locales);
        })
        .catch(error=>{
            console.log(error);
        })
    }
    handleClick(event) {
      this.setState({
        actualpagina: Number(event.target.id)
      });
    }
    render() {
      // if(this.state.locales.unidad != undefined){
                const {locales,actualpagina,todosporpagina} =this.state;
                const indexOfLastTodo = actualpagina * todosporpagina;
                const indexOfFirstTodo = indexOfLastTodo - todosporpagina;
                const actualTodos = locales.slice(indexOfFirstTodo, indexOfLastTodo);
                const renderTodos = actualTodos.map((todo, index) => {
                  return <li key={index}>{todo}</li>;
                });
                const pageNumbers = [];
                
                for (let i = 1; i <= Math.ceil(locales.length / todosporpagina); i++) {
                  pageNumbers.push(i);}
                const url="https://mercadobodegas.cl";
                const url2="https://www.mercadobodegas.cl/almacenes/public/img/instalaciones";
                
                                return (
                          <div>
                              <Header logo={logo_header} 
                                item1="Bodegas cerca tuyo"
                                item2="Guía de tamaños"
                                item3="Contacto"/>  
                              <div>
                                <section   className="mt-5 mb-5">
                                  <div   className="container">
                                    <div   className="row margin">
                                      <div   className="col-md-3">
                                        <div   className="mb-4" id="mapa-button">
                                          <img 
                                            alt="mapa2"
                                            className="h-100 w-100"
                                            src={url+"/assets/mapa2.svg"}
                                            // src="https://www.muypymes.com/wp-content/uploads/2018/03/googlemaps-googledevelopers1-660x330.png"
                                          />
                                          
                                          <a href="#myModal" role="button" class="btn btn-block bg-orange white" data-toggle="modal">Ver Resultados en el Mapa</a>
                                            
                                          
                                        </div>
                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            
                
                <button type="button" class="btn anterior" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">Regresar</span>
                </button>
            
            <div class="modal-body" id="result">
                
                <div class="row">
                    
                      <Mapa></Mapa>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
<button type="button" class="btn oculto-btn btnfiltro center-block bg-orange" data-toggle="modal" data-target="#exampleModal">
        Filtros
      </button>
      <div class="modal   fade oculto modalexp" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content altura">
           
            <div class="modal-body">
                                <button type="button" class="btn filtro bg-orange  btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <div  id="filtros" className="filtrosresponsive slideshow-container">
                                          <div   className="filtro-contenido">
                                            <h3 >Filtros</h3>
                                            <div  id="filtro-tamaño">
                                              <p >Tamaño</p>
                                              {/* filtros laterales*/}
                                             
                                              
                                              
                                              
                                              <ul className="grupo-filtro">
                                              
                                              
                                                <btn onClick={() => this.filtro(3)}>
                                                  <Filtrolateral
                                                  id="filtro1"
                                                  medida="3m"
                                                  />
                                                </btn>
                                                <btn onClick={this.filtro.bind(this,5)}>
                                                  <Filtrolateral
                                                    id="filtro2"
                                                    medida="5m"
                                                  />
                                                </btn>
                                                <btn onClick={this.filtro.bind(this,7)}>
                                                  <Filtrolateral
                                                    id="filtro3"
                                                    medida="7m"
                                                  />
                                                </btn>
                                                <btn onClick={this.filtro.bind(this,10)}>
                                                  <Filtrolateral
                                                    id="filtro4"
                                                    medida="10m"
                                                  />
                                                </btn>
                                                <btn onClick={this.filtro.bind(this,15)}>
                                                <Filtrolateral
                                                id="filtro5"
                                                medida="15m"
                                                
                                                />
                                                </btn>
                                                <btn onClick={this.filtro.bind(this,20)}>
                                                <Filtrolateral
                                                id="filtro6"
                                                medida="20m"
                                                
                                                />
                                                

                                                </btn>
                                                
                                                <btn onClick={this.filtro.bind(this,30)}>
                                                <Filtrolateral
                                                id="filtro6"
                                                medida="30m"
                                                
                                                />
                                                </btn>
                                                
                                                
                                              </ul>
                                              
                                              
                                              
                                              
                                              <div     className="oculto bg-white">
                                                
                                              </div>
                                            </div>
            </div>
           
          </div>
        </div>
      </div>
                                        
                                            <div
                                                
                                              className="mt-3"
                                              id="filtro-amenidades"
                                            >
                                              <p
                                                  
                                                className="oculto sf-type sf-type-tiny sf-type-gray sf-type-eyebrow"
                                              >
                                                Características
                                              </p>
                                              <ul     className=" oculto grupo-filtro">
                                                  
                                                <li
                                                    
                                                  className="checkbox-item ng-star-inserted"
                                                >
                                                  <div
                                                      
                                                    className=" oculto custom-control custom-checkbox"
                                                  >
                                                    <input
                                                        
                                                      className="custom-control-input ng-untouched ng-pristine ng-valid"
                                                      type="checkbox"
                                                      id="amenidad1"
                                                    /><label
                                                        
                                                      className="custom-control-label"
                                                      htmlFor="amenidad1"
                                                    >
                                                      Clima controlado
                                                    </label>
                                                  </div>
                                                </li>
                                                <li
                                                    
                                                  className="checkbox-item ng-star-inserted"
                                                >
                                                  <div
                                                      
                                                    className="custom-control custom-checkbox"
                                                  >
                                                    <input
                                                        
                                                      className="custom-control-input ng-untouched ng-pristine ng-valid"
                                                      type="checkbox"
                                                      id="amenidad2"
                                                    /><label
                                                        
                                                      className="custom-control-label"
                                                      htmlFor="amenidad2"
                                                    >
                                                      Acceso 24 horas
                                                    </label>
                                                  </div>
                                                </li>
                                                <li
                                                    
                                                  className="checkbox-item ng-star-inserted"
                                                >
                                                  <div
                                                      
                                                    className="custom-control custom-checkbox"
                                                  >
                                                    <input
                                                        
                                                      className="custom-control-input ng-untouched ng-pristine ng-valid"
                                                      type="checkbox"
                                                      id="amenidad3"
                                                    /><label
                                                        
                                                      className="custom-control-label"
                                                      htmlFor="amenidad3"
                                                    >
                                                      1er Piso
                                                    </label>
                                                  </div>
                                                </li>
                                              </ul>
                                            </div>
                                          </div>
      <div    id="btn-apply-filters">
        <button
            
            className="oculto btn btn-block bg-orange black"
          id="aplicar-filtros"
        >
          <b   >Aplicar Filtros</b>
        </button>
      </div>
    </div>
  </div>
  <div     className="col-md-9">
      <div
        className="text-center p-5 bg-grey"
        id="busqueda"
      >
      <h3  >
        Bodegas disponibles en
        <span   
          >Santiago, Región Metropolitana</span
        >.
      </h3>
        
      <h5     className="mb-3">
        Encuentra bodegas cerca tuyo.
      </h5>
      <form
          
          className="form-inline my-2 my-lg-0 row text-center ng-untouched ng-pristine ng-valid"
        noValidate=""
      >
        
        <div
            className="search col-md-12 col-xs-12 pr-1 pl-0">
           <Search 
           clasebutton="btn btn-block bg-orange white my-2 my-sm-0"
  titulo="Todas las mini bodegas en un solo lugar."
  subtitulo="Cotiza, compara y reserva sin compromiso."
  placeholder="Ingresa dirección, calle, comuna, o ciudad."
  txtbutton="Buscar"
  />
        </div>
       
        
      </form>
    </div>
    
    <div
        
        className="text-center mt-2"
      id="settings-mobile"
    >
    </div>
    <div
        
        className="d-flex flex-row justify-content-between align-items-center form-inline"
    >
      <div     className="mt-3 mb-3">
        1 - 10 de 13 bodegas cerca de Santiago, Región Metropolitana
      </div>
      <div     className="mt-3 mb-3">
        <div     className="form-group">
          <label
              
              className="mr-3"
              htmlFor="select-order"
            >Ordenar por</label
          ><select
              
              className="form-control"
            id="select-order"
            name="select-order"
            ><option    value="distance">Distancia</option><option    value="precio">Precio</option>
            </select>
        </div>
      </div>
    
      </div>
      {/* aqui empieza el contenedor */}
      


          <div  className="element p-4 border-grey mt-4 mb-4"   >
          { 
         
                  actualTodos.map(
                      local=>{
                    return <div className="row border border-grey mb-3" key={local.local_id}> 
                              <div  className="container mt-5 mb-5" >
                                <div className="row">
                                  <div className="col-sm-4">
                                    <a>
                                      <img
                                        alt=""
                                        // https://www.mercadobodegas.cl/almacenes/public/img/instalaciones/16/MiniBodegasSantiago_20-01-56_2020-01-20_1.jpg
                                        className="img-responsive w-100 imagen-almacen ng-star-inserted"
                                        src={url2+"/16/MiniBodegasSantiago_20-01-56_2020-01-20_1.jpg"}
                                      />
                                    </a>
                                  </div>
                                  <div className="col-sm-8 mt-xs-3">
                                    <h4>{local.local_nombre}</h4>
                                    <h6>{local.local_direccion}</h6>
                                    <h6 className="mt-3">Telefono: {local.local_telefono} </h6>
                                    <div>
                                    <ul className="medidas">
                                        {local.unidad.map(unidad=>{
                                                  return (
                                                  
                                                    this.state.numero==0?
                                                      <li key={unidad.unidad_id} className="">
                                                        <a className="">
                                                          <div className="unidad d-flex justify-content-between align-items-center align-self-center ">
                                                            <div>
                                                              <div className="areatexto">
                                                                    <h6 className="border-bottom border-black d-inline">
                                                                      <b> {unidad.unidad_area}m
                                                                        <sup>2</sup>
                                                                      </b>
                                                                    </h6>
                                                                    
                                                                    
                                                              </div>
                                                              <div className="oferta">
                                                                <b className="cyan"> {unidad.unidad_oferta}</b>
                                                                
                                                              </div>
                                                            </div>
                                                            <div>
                                                              <h6>
                                                                <b className="precio">
                                                                  {unidad.unidad_precioMensual}
                                                                </b>
                                                                <i className=" fa fa-chevron-right float-right ml-2 cyan" aria-hidden="true"></i>
                                                              </h6>
                                                            </div>
                                                          </div>
                                                        </a>
                                                      </li>:
                                                      unidad.unidad_area>=this.state.numero[0] && unidad.unidad_area <= this.state.numero[1] ?
                                                      <li key={unidad.unidad_id} className="">
                                                        <a className="">
                                                          <div className="unidad d-flex justify-content-between align-items-center align-self-center ">
                                                            <div>
                                                              <div className="areatexto">
                                                                    <h6 className="border-bottom border-black d-inline">
                                                                      <b> {unidad.unidad_area}m
                                                                        <sup>2</sup>
                                                                      </b>
                                                                    </h6>
                                                                    
                                                                    
                                                              </div>
                                                              <div className="oferta">
                                                                <b className="cyan"> {unidad.unidad_oferta}</b>
                                                                
                                                              </div>
                                                            </div>
                                                            <div>
                                                              <h6>
                                                                <b className="precio">
                                                                
                                                                  {unidad.unidad_precioMensual}
                                                                </b>
                                                                <i class="fas fa-chevron-right"></i>
                                                              </h6>
                                                            </div>
                                                          </div>
                                                        </a>
                                                      </li>
                                                      :
                                                      null
                                          
                                          )
                              
                                                    })}
                                    </ul>
                                    
                                    <a  className="btn float-right bg-orange white"
                                       href={"/detalles/"+local.local_id}>
                                         Ver detalles
                                      </a>
                                    </div>
                                  </div>          
                                </div>
                              </div>
                            </div>
                                                }
                                                
                                                )
                                        }
                                      

                                      
          </div>
                                      
                                  
                                
                            

                                </div>
                                </div>
                                </div>
                              </section>
                          </div>
                          <div className="paginador">
                          {
                        pageNumbers[0]!=undefined?
                        
                        pageNumbers.map(number => {
                          return (
                            
                            <button className="btn btn-pag bg-orange"
                              key={number}
                              id={number}
                              onClick={this.handleClick}
                            >
                              {number}
                            </button>
                          );
                        })
                        
                        :"vete al carajo"
                          }</div>
                          <Footer
                          item1="¿Necesitas ayuda?"
                          item2="Contacto"
                          item3=" contacto@mercadobodegas.net"
                          item4="09807169267"
                          item5="Para dueños de Mini Bodegas"
                          item6="Manual de Uso"
                          item7="Iniciar Sesión"
                          item8="MercadoBodegas en:"
                          item9="© MercadoBodegas 2019 | Derechos Reservados"
                          logo={logo_footer}
                          />
                          
                          </div>
                                
                                )
                                {/* }
                                 else{
                              return  null;
                            
                            
                            }  */}
                            }
                           
                          }
