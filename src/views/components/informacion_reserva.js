import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class informacion_reserva extends Component {
    render() {
        return (
            <div className="card mb-3 bg-white">
               <div  className="card-body">
                    <h3 >
                        Tu información
                    </h3>
                    <span>{this.props.nombre}</span>
                    <br></br>
                    <span>{this.props.email}</span>
                    <br></br>
                    <span>{this.props.telefono}</span>
                    <br></br>
                    <Link to={"/actualizar-reserva/"+this.props.id}>
                    <button className="btn btn-reserva bg-orange black mt-3">
                        <b>Actualiza tu información</b>
                    </button>
                    </Link>
                    <h3 className="mt-3">
                        Instalación
                    </h3>
                    <span>{this.props.unidad}</span>
                    <br ></br>
                    <span>{this.props.direccion}</span>
                    <br></br>
                    <span>{this.props.region}</span>
                    <br></br>
                    <span>{this.props.telefonounidad}</span>
                    <br></br>
                    <img  alt="imagen" className="imagen-reserva" 
                    src="http://d2knwvu6cegzt1.cloudfront.net/large-compress/2154145e4273f26e369.jpg"/>
                        <br></br>
                        <button   className="btn btn-reserva bg-orange black mt-3">
                                <b>Cancelar Reserva</b>
                        </button>
                        </div>
                
            </div>
        )
    }
}
