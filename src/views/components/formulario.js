import React, { Component } from 'react'
import axios from 'axios'
import {Redirect} from 'react-router-dom'
export default class formulario extends Component {
    state = {
        nombre: '',
        apellido: '',
        email: '',
        telefono: '',
        fechaMudanza: '',
        id: 0,
        respuesta:{}
        
    }
    handleChange = event => {
        if(event.target.name=="nombre"){
            this.setState({ nombre: event.target.value });
            console.log(this.state.nombre);
        }
        if(event.target.name=="apellido"){
            this.setState({ apellido: event.target.value });
            
        }
        if(event.target.name=="email"){
            this.setState({ email: event.target.value });
            console.log(this.state.nombre);
        }
        if(event.target.name=="telefono"){
            this.setState({ telefono: event.target.value });
            console.log(this.state.nombre);
        }
        if(event.target.name=="date"){
            this.setState({ fechaMudanza: event.target.value });
            console.log(this.state.nombre);
        }

        
      }
    handleSubmit = event => {
        event.preventDefault();
        const reserva = {
                reserva_nombre: this.state.nombre,
                reserva_apellido: this.state.apellido,
                reserva_email: this.state.email,
                reserva_telefono: this.state.telefono,
                reserva_fechaMudanza: this.state.fechaMudanza,
                unidad_id: this.props.unidad_id
    
        };
        var myJsonString = JSON.stringify(reserva);
        // axios.post(`https://jsonplaceholder.typicode.com/users`, { usuario })
        // axios.post(`http://192.168.1.14/z_proyectosLaravel/MercadoBodegasDos/public/api/reservas/insertarReserva`, JSON.stringify(usuario))
        console.log(myJsonString);
        
        // axios.post(`http://34.95.219.186/AMdigital_MercadoBodegas/public/api/reservas`, myJsonString)
        // console.log(myJsonString);
        axios.post(`http://amdigital.tech/api/reservas`, myJsonString)
          .then(res => {
              console.log(res.data)
            this.setState({respuesta:res.data})
            
          })
      }
    render() {
        return (
<div>
                <form className="formulario-reserva" onSubmit={this.handleSubmit}>
                <h4>Haz tu reserva</h4>
                     <div className="form-group">
                        <input 
                            type="text"
                            className="form-control"
                            
                            name ="nombre"
                            placeholder="Nombre"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="text"
                            className="form-control"
                            
                            name ="apellido"
                            placeholder="Apellido"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="email"
                            className="form-control"
                            
                            name ="email"
                            placeholder="Email"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <input 
                            type="tel"
                            className="form-control"
                            
                            name ="telefono"
                            placeholder="Telefono"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group"> 
                        <input className="form-control"  name="date" placeholder="MM/DD/YYY" type="date"
                        onChange={this.handleChange}/>
                    </div>
                    <button  type="submit" className="btn-reserva btn btn-primary">Reservar</button>
                    
                    
                </form>    
                
               {this.state.respuesta.items!=undefined?
               this.state.respuesta.items[0].mensaje=="reserva creada con exito"?
               <div>
                   {console.log(this.state.respuesta)}
                    <Redirect to={"/reserva/"+this.state.respuesta.items[0].cuerpo.reserva_codigo}/>
               </div>:
                    this.state.respuesta.status=="ERROR"?
                    alert(this.state.respuesta.items[0].mensaje):null
               :null
                
            }
                </div>            

        )
        
    }
}
                        
