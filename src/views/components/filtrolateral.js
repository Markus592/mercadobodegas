import React, { Component } from 'react'

export default class filtrolateral extends Component {
    render()
     {
        const url="https://mercadobodegas.cl";
        return (
            
                    <li
                        className="item-filtro ">
                        <button
                            className="w-100 btn btn-medidas border border-grey"
                            type="button"
                            id={this.props.id}
                            >
                            <span
                            className="d-inline-flex align-center">
                                <span className="icon">
                                <img
                                    className="ph-image unit-icon"
                                    height="50px"
                                    width="50px"
                                    alt=""
                                    src={url+"/almacenes/public/img/img_1.svg"}/>
                                </span>
                                <span>
                                <p className="mt-3">
                                    {this.props.medida}
                                    <sup>2</sup>
                                </p>
                                </span>
                            </span>
                        </button>
                    </li>
                   
            
        )
    }
}
