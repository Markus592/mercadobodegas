import React, { Component } from 'react'
import GooglePlacesAutocomplete from 'react-google-places-autocomplete'; 
import { Redirect } from 'react-router';
 class Search extends Component {
  state={
    redirect:''
  }
  redirecciona(){
    if (this.state.redirect!='') {
      return <Redirect to={'/locales/'+this.state.redirect}/>
    } 
  }
  
  setRedirect = (event) => {
    console.log(event);
    this.setState({
      redirect: event.description
    })
  }
    render() {
        return (
            <React.Fragment>
                
                    
                      <div  className="col-sm-6 col-xs-12 pr-1 pl-0">
                        {this.redirecciona()}
                        
                        <GooglePlacesAutocomplete
                          onSelect={this.setRedirect}
                          // onChange={this.handleChange}
                          
                          placeholder='Ingresa dirección, calle, comuna, o ciudad.'
                          autocompletionRequest={{
                          componentRestrictions: {
                            country: ['Cl'],
                          }
                          }}
                          
                        />
                      
                      </div>
                      <div  class="col-md-2 col-xs-12 pl-0 pr-0">
                            <a href="/locales"
                          className={this.props.clasebutton}>
                          {this.props.txtbutton}
                        </a>
                      </div>
                  
                
                
              </React.Fragment>
              
            

            
        )
    }
}
export default Search