import React, { Component } from 'react'
export default class updateReserva extends Component
{
    constructor(props)  {
                        super(props);
                        }
    click(){
        this.props.click();
    }
    render() {
        return (
            <div className="wrapper">
                <form className="form-signin" onSubmit={this.handleSubmit}>
                        <h2 className="form-signin-heading">{this.props.titulo} </h2>
                        <input type={this.props.type1} className="form-control" name={this.propsname1} placeholder={this.props.plac1} required="" autoFocus="" />
                        <input type={this.props.type2} className="form-control" name={this.propsname2} placeholder={this.props.plac2} required=""/>      
                        <input type={this.props.type3} className="form-control" name={this.propsname3} placeholder={this.props.plac3} required=""/>      
                        <button   className="btn updreserva btn-lg bg-orange btn-block" onClick={this.click.bind(this)} type="submit" >{this.props.namebtn}  </button>
                </form>
            </div>
        )
    }
}
