import React, { Component } from 'react'

export default class footer extends Component {
    render() {
        return (
                <footer  className="bg-black white pb-1 pt-4">
                  <div  className="container">
                    <div  className="row mt-2">
                      <div 
                      className="col-sm-4 mb-2 d-flex flex-column align-items-xs-center"
                      >
                        <h6 >{this.props.item1}</h6>
                        <h6  className="mt-2 mb-3 orange">
                          {/* <a
                            
                            aria-expanded="false"
                            aria-haspopup="true"
                            className="link-border border-orange orange clickable"
                            data-toggle="dropdown"
                            id="contactoFooter"
                          > */}
                            <b>{this.props.item2}</b>
                            <div  className="line">
                            </div>
                          {/* </a> */}
                          <div
                            aria-labelledby="contacto"
                            className="dropdown-menu dropdown-menu-right dropdown-custom"
                          >
                            <a
                              
                            className="dropdown-item dropdown-item-custom"
                              href="#"
                              ><i _ngcontent-serverapp-c2="" className="far fa-envelope"></i>
                              {this.props.item3} </a
                            ><a
                              
                            className="dropdown-item dropdown-item-custom"
                              href="#"
                              ><i  className="fas fa-phone-alt"></i> {this.props.item4}
                            </a>
                          </div>
                        </h6>
                      </div>
                      <div
                        
                      className="col-sm-4 mb-2 d-flex flex-column align-items-xs-center"
                      >
                        <h6 >{this.props.item5}</h6>
                        <a  className="mt-2" href="#"
                          ><span  className="link-border border-white"
                            ><b>{this.props.item6}</b></span>
                            </a>
                            <a  className="clickable"href="#"
                          ><span className="link-border border-white"
                            ><b >{this.props.item7}</b></span></a>
                             <a  className="clickable" href="/login-reserva"
                          ><span className="link-border border-white"
                            ><b >Tu reserva</b></span></a>
                      </div>
                      <div
                        
                      className="col-sm-4 mb-2 d-flex flex-column align-items-xs-center"
                      >
                        <h6 >{this.props.item8}</h6>
                        <div  className="mt-3">
                          <div  className="row redes">
                            <div  className="circulo">
                              <i  className="fa fa-facebook"></i>
                            </div>
                            <div  className="circulo">
                              <i  className="fa fa-twitter"></i>
                            </div>
                            <div  className="circulo">
                              <i  className="fa fa-linkedin"></i>
                            </div>
                            <div  className="circulo">
                              <i  className="fa fa-youtube"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div  className="row linea mt-3 mb-3"></div>
                    <div  className="row pb-2">
                      <div  className="col-md-4  mb-3">
                        <div
                          
                        className="logo-footer d-flex align-items-center justify-content-start align-items-xs-center align-items-only-sm-center"
                          
                          ><img
                            
                            alt="Mercado Bodegas"
                            src={this.props.logo}
                            
                        /></div>
                      </div>
                      <div
                        
                        className="col-md-4 d-flex align-items-center justify-content-end align-items-xs-center mb-2"
                      ></div>
                      <div
                        
                        className="col-md-4 d-flex align-items-center justify-content-center mb-2 text-xs-center"
                      >
                        <h6 >
                        {this.props.item9}
                        </h6>
                      </div>
                    </div>
                  </div>
                </footer>
                            
                        )
                    }
                }
