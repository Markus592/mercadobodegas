import React from 'react'

 class Navbar extends React.Component {
    
    
     render()
     {
    return (
        <nav className=" bg-orange navbar navbar-expand-lg navbar-light bg-light">
         <a
    className="navbar-brand padding-logo mr-0"
    href="/"
    >
      <img
        
        alt="Mercado Bodegas"
        src={this.props.logo}
        
      />
      </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span className="navbar-toggler-icon"></span>
        </button>
        
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
         <ul className="navbar-nav ml-auto">
           <li className="link hover-1  link-wrapper nav-item ml-3 mr-3">
             <a className=" nav-link" href="#">{this.props.item1} <span className="sr-only">(current)</span></a>
           </li>
           <li className="link hover-1 link-wrapper nav-item ml-3 mr-3">
             <a className=" nav-link" href="#">{this.props.item2}</a>
           </li>
           <li className="link hover-1 link-wrapper nav-item ml-3 mr-3">
             <a className=" nav-link" href="#">{this.props.item3}</a>
           </li>
       
          
         </ul>
        
        </div>
        </nav>
        )
}
    
}
export default Navbar;

