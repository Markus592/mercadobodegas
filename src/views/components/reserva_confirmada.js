import React, { Component } from 'react'

export default class reserva_confirmada extends Component {
    render() {
        return (
            
                <div className="row">
                    <div className="col-md-12">
                        <div className="card-body bg-white">
                            <div className="row">
                                <div className="col-md-8">
                                <h1>Tu reserva esta confirmada</h1>
                                <h3>
                                Código de confirmación: 
                                    <b>{this.props.codigo}</b>
                                </h3>
                                <span>tu fecha de mudanza</span>
                                    <h4>{this.props.fecha}</h4>
                                    {/* <br></br> */}
                                    {/* <button className="btn-reserva btn bg-orange black mt-2">
                                        <b>Cambiar fecha de mudanza</b>
                                    </button> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        )
    }
}
