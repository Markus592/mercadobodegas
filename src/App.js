import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
  
} from "react-router-dom";
import Viewindex from './views/pag-main'
import Locales from './views/locales'
import Detalles from './views/services/services_detalles'
import Formulario from './views/components/formulario'
import Reserva from './views/services/reserva.js'
import UpdReserva from './views/services/upddetalle'
import LoginReserva from './views/LogReserva'

import Mapa from './views/mapa'
import Get from './views/get'
import Post from './views/formulario'
import Meta from './views/metas'

// import Filtro from './views/filtros'
class App extends React.Component {
  render()
  {
    
 return ( 
   <div>
     
  <Router>
  <Switch>
    <Route exact path="/">
    <Viewindex/>
    </Route>
    <Route path="/locales">
        <Locales/>
    </Route>
    
        
          
        
    {/* <Route path="/detalles">
        <Detalles/>
    </Route> */}
    <Route path="/formulario">
        <Formulario/>
    </Route>
    <Route path="/reserva/:id" children={<Reserva/>} />
    
    <Route path="/login-reserva">
    <LoginReserva/>
    </Route>
    <Route path="/actualizar-reserva/:id" children={<UpdReserva/>} />
    <Route exact path="/mapa">
    <Mapa/>
    </Route>
    
    <Route path="/detalles/:id" children={<Detalles/>} />
    <Route exact path="/post">
    <Post/>
    </Route>
    
    
    
    
  </Switch>
</Router>
<div>
  
   
  </div>
  </div>
)
}
 
}
export default App;
